var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'marshalls-game', { preload: preload, create: create, update: update, render: render });

var mainCharacter;
var enemy;
var Keys = Phaser.Keyboard;
var speed = 4;
var style = 'default';
var cursors;
var projectiles;
var bullets;
var rockets;
var jumpTimer = 0;

var jumpCount = 0;
var w = 800, h = 600;
var facingRight;

var allWeapons = {
    'AR':                   { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 40, bulletSpread: 5, animation: [0,1,2,2,1,0]},
    'Rocket launcher':      { bullets: 'rockets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 5, bulletFireLimit: 4, bulletSpread:  2, animation: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,0]},
    'AR + scope':           { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 50, bulletSpread: 2, animation: [0,1,2,2,1,0]},
    'AR + scope + laser':   { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 60, bulletSpread: 1, animation: [0,1,2,2,1,0]},
    'shotgun':              { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 60, bulletSpread: 10, animation: [0,1,2,3,2,1,0]},
    'shotgun + laser':      { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 55, bulletSpread: 8, animation: [0,1,2,3,2,1,0]},
    'minigun':              { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 100, bulletFireLimit: 165, bulletSpread: 15, animation: [0,1,2,3,4,5,0]},
    'sniper':               { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -10, bulletFireRate: 1, bulletFireLimit: 1, bulletSpread: 0, animation: [0,1,2,3,4,0]},
    'ultra gun':            { bullets: 'bullets', scaleX: -0.75, scaleY: 0.75, bulletOffsetX: 40, bulletOffsetY: -20, bulletFireRate: 100, bulletFireLimit: 85, bulletSpread: 1, animation: [0,1,2,3,4,5,6,0]},
}

var currentWeapon = null;
var currentWeaponName = null;

var allowedToFire = true;

function preload() {

    game.load.image('ground', 'assets/backgrounds/ground-2x.png');
    game.load.image('river', 'assets/backgrounds/river-2x.png');
    game.load.image('sky', 'assets/backgrounds/sky-2x.png');
    game.load.image('cloud0', 'assets/backgrounds/cloud-big-2x.png');
    game.load.image('cloud1', 'assets/backgrounds/cloud-narrow-2x.png');
    game.load.image('cloud2', 'assets/backgrounds/cloud-small-2x.png');

    game.load.tilemap('mario', 'assets/tilemaps/maps/super_mario.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('tiles', 'assets/tilemaps/tiles/super_mario.png');

    game.load.spritesheet('AR', 'assets/sprites/Startinggun.png', 64, 64, 4);
    game.load.spritesheet('Rocket launcher', 'assets/sprites/Rocket launcher v1.png', 64, 64, 20);
    game.load.spritesheet('AR + scope', 'assets/sprites/Standard gun [g1] (1).png', 64, 64, 4);
    game.load.spritesheet('AR + scope + laser', 'assets/sprites/Standard gun [g1] (2).png', 64, 64, 4);
    game.load.spritesheet('shotgun', 'assets/sprites/Standard gun [g1] (3).png', 64, 64, 4);
    game.load.spritesheet('shotgun + laser', 'assets/sprites/Standard gun [g1] (4).png', 64, 64, 4);
    game.load.spritesheet('minigun', 'assets/sprites/Standard gun [g1] (5).png', 64, 64, 6);
    game.load.spritesheet('sniper', 'assets/sprites/Standard gun [g1] (6).png', 64, 64, 6);
    game.load.spritesheet('ultra gun', 'assets/sprites/Standard gun [g1] (7).png', 64, 64, 9);

    game.load.spritesheet('bullet', 'assets/sprites/Bullet.png', 128,128,4);
    game.load.spritesheet('rocket', 'assets/sprites/Rocket v1.png', 32,32,4);
    game.load.atlasJSONHash('mainCharacter', 'assets/sprites/running_bot.png', 'assets/sprites/running_bot.json');

    game.load.spritesheet('flyinglaser', 'assets/sprites/flyinglaser.png', 32,32,36);

    game.load.image('menu', 'assets/buttons/number-buttons-90x90.png', 270, 180);

}

function create() {

    //  Make the world larger than the actual canvas
    game.world.setBounds(0, 0, w*3, h);
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.gravity.y = 400;

    //  Background images
    game.add.tileSprite(0, 0, 1400, h, 'sky');
    game.add.sprite(0, 360, 'ground');
    game.add.sprite(0, 400, 'river');
    game.add.sprite(200, 120, 'cloud0');
    game.add.sprite(-60, 120, 'cloud1');
    game.add.sprite(900, 170, 'cloud2');

    map = game.add.tilemap('mario');
    map.addTilesetImage('SuperMarioBros-World1-1', 'tiles');
    layer = map.createLayer('World1');
    layer.resizeWorld();

    enemy = game.add.sprite(500, 600, 'flyinglaser');
    enemy.anchor.setTo(0.5, 0.5);
    enemy.scale.setTo(-2, 2);
    enemy.animations.add('explode', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14], 30, false);
    enemy.animations.add('fire', [15,16,17,18,19,20,21,22,23,24,25,26], 20, false);
    enemy.animations.add('fly', [27,28,29,30,31], 20, false);
    game.physics.enable(enemy, Phaser.Physics.ARCADE);
    enemy.body.collideWorldBounds = true;
    enemy.body.bounce.y = 0.2;
    enemy.animations.play('explode', 30, true);

    //
    mainCharacter = game.add.sprite(300, 600, 'mainCharacter');
    mainCharacter.anchor.setTo(0.5, 0.5);
    mainCharacter.scale.setTo(-2, 2);
    facingRight = true;

    mainCharacter.animations.add('run');
    game.physics.enable(mainCharacter, Phaser.Physics.ARCADE);
    mainCharacter.body.collideWorldBounds = true;
    mainCharacter.body.bounce.y = 0.2;

    //registration point
    //  Creates 30 bullets, using the 'bullet' graphic
    bullets = game.add.weapon(30, 'bullet');
    bullets.addBulletAnimation('fire', [0,1,2], 20, true)
    bullets.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
    bullets.bulletSpeed = 400;
    bullets.bulletGravity.y = -400;
    bullets.onFireLimit.add(resetFireLimit, this);

    rockets = game.add.weapon(4, 'rocket');
    rockets.addBulletAnimation('fire', [0,1,2,3], 20, true)
    rockets.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
    rockets.bulletSpeed = 400;
    rockets.bulletGravity.y = -400;
    rockets.onFireLimit.add(resetFireLimit, this);

    function resetFireLimit(weapon, limit) {
        allowedToFire = false;
        setTimeout(function() { weapon.resetShots(); allowedToFire = true;}, 2500);
    }

    chooseWeapon('AR')

    game.camera.follow(mainCharacter, Phaser.Camera.FOLLOW_PLATFORMER);

    cursors = game.input.keyboard.createCursorKeys();


    // Create a label to use as a button
    pause_label = game.add.text(15, 15, 'Menu', { font: '18px Arial', fill: '#fff' });
    pause_label.inputEnabled = true;
    pause_label.events.onInputUp.add(function () {
        // When the pause button is pressed, we pause the game
        game.paused = true;

        // Then add the menu
        menu = game.add.sprite(w/2, h/2, 'menu');
        menu.anchor.setTo(0.5, 0.5);

        pause_label.text = 'Return to Game'
     });

    // Add a input listener that can help us return from being paused
    game.input.onDown.add(unpause, self);

    // And finally the method that handels the pause menu
    function unpause(event){
        // Only act if paused
        if(game.paused){
            // Calculate the corners of the menu
            var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
                y1 = h/2 - 180/2, y2 = h/2 + 180/2;

            // Check if the click was inside the menu
            if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                // The choicemap is an array that will help us see which item was clicked
                var choicemap = ['Switch Weapon', 'two', 'three', 'four', 'five', 'six'];

                // Get menu local coordinates for the click
                var x = event.x - x1,
                    y = event.y - y1;

                // Calculate the choice
                var choice = Math.floor(x / 90) + 3*Math.floor(y / 90);

                // Display the choice
                 if (choice == 0) {
                    keys = Object.keys(allWeapons)
                    index = keys.indexOf(currentWeaponName);
                    index = index + 1
                    if (index >= keys.length) {
                        index = 0
                    }
                    chooseWeapon(keys[index])
                     pause_label.text = 'Weapon: ' + keys[index];
                 }
            }
            else{
                // Remove the menu and the label
                menu.destroy();
                // Unpause the game
                game.paused = false;
                pause_label.text = 'Menu'
            }
        }
    }
}

function chooseWeapon(weaponName) {

    if (currentWeapon) {
        mainCharacter.removeChild(currentWeapon)
    }
    currentWeaponName = weaponName
    var weaponInfo = allWeapons[weaponName]
    currentWeapon = game.add.sprite(-20, 0, weaponName)
    currentWeapon.anchor.setTo(0.5, 0.5);
    currentWeapon.scale.setTo(weaponInfo['scaleX'],weaponInfo['scaleY']);
    currentWeapon.animations.add('fire', weaponInfo['animation'])
    mainCharacter.addChild(currentWeapon);
    projectiles = weaponInfo['bullets'] == 'bullets' ? bullets : rockets;
    projectiles.bulletAngleVariance = weaponInfo['bulletSpread'];
    projectiles.fireLimit = weaponInfo['bulletFireLimit'];
    projectiles.fireRate = weaponInfo['bulletFireRate'];

    if (facingRight) {
        projectiles.trackSprite(currentWeapon, weaponInfo['bulletOffsetX'], weaponInfo['bulletOffsetY'], true);
    } else {
        projectiles.trackSprite(currentWeapon, -weaponInfo['bulletOffsetX'], weaponInfo['bulletOffsetY'], true);
    }

}


function update() {


    var weaponInfo = allWeapons[currentWeaponName]
    if (cursors.left.isDown) {
        mainCharacter.x -= speed;
        mainCharacter.scale.x = 2;
        mainCharacter.animations.play('run', 10, true);
        facingRight = false;
        projectiles.bulletSpeed += speed;
        projectiles.trackSprite(currentWeapon, -weaponInfo['bulletOffsetX'], weaponInfo['bulletOffsetY'], true);
    } else if (cursors.right.isDown) {
        mainCharacter.x += speed;
        mainCharacter.scale.x = -2;
        mainCharacter.animations.play('run', 10, true);
        facingRight = true;
        projectiles.bulletSpeed += speed;
        projectiles.trackSprite(currentWeapon, weaponInfo['bulletOffsetX'], weaponInfo['bulletOffsetY'], true);
    } else {
        mainCharacter.animations.stop();
        projectiles.bulletSpeed = 400;
    }

    if (cursors.up.isDown && jumpCount < 2 && game.time.now > jumpTimer ) {
        mainCharacter.body.velocity.y = -250;
        jumpTimer = game.time.now + 100;
        jumpCount++;
    }

    if (mainCharacter.body.onFloor()) {
        jumpCount = 0;
    }

    if (game.input.activePointer.isDown && allowedToFire)
    {
        currentWeapon.animations.play('fire', 30)
        projectiles.fireAtPointer(game.input.activePointer);
    }

}

function render () {

}